/******************************************************************************
 * TickZero.cpp
 * Copyright (c) 2017 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * One second tick generator using the Arduino Zero ATSAMD21 RTC peripheral.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include "Arduino.h"


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "TickZero.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define RTC_SYNC() while (RTC->MODE1.STATUS.bit.SYNCBUSY) {}
#define FRAC_DENOM (0.00006103515625)  // 1.0/16384.0


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
TickZero_ TickZero;  //!< Global TickZero object for the application to use
 
 
/******************************************************************************
 * Local data.
 ******************************************************************************/
static TickZeroIsrPtr RTC_callBack = 0;


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/
 
/**************************************
 * TickZero_::TickZero
 **************************************/
TickZero_::TickZero_()
{
    mConfigured = false;
}


/**************************************
 * TickZero_::begin
 **************************************/
void TickZero_::begin()
{    
    uint16_t ctrl_reg = 0;
    
    if (!mConfigured)
    {
        // Clock configuration.
        PM->APBAMASK.reg |= PM_APBAMASK_RTC; // turn on digital interface clock
        config32kOSC();
        configClock();
        
        // Place all RTC registers in their initial state.
        RTC->MODE1.CTRL.reg = 0;
        RTC_SYNC()
        RTCreset();
        RTCresetRemove();

        // Disable continuous read synchronization.
        // Not sure if it is needed - it is carried over from example code.
        //RTC->MODE1.READREQ.reg &= ~RTC_READREQ_RCONT;
        
        // Mode 1: 16-bit counter @ 16384Hz (Frequency set by configClock())
        ctrl_reg = RTC_MODE1_CTRL_MODE_COUNT16 |   // Mode 1: 16-bit counter
                   RTC_MODE1_CTRL_PRESCALER_DIV1;  // 1:1 prescaler, clk = 16384Hz
                   
        RTC->MODE1.CTRL.reg = ctrl_reg;
        RTC_SYNC()
        
        // Period register generates an interrupt every second.
        RTC->MODE1.PER.reg = RTC_MODE1_PER_PER(16383);
        RTC_SYNC()
        
        // Enable RTC interrupts in the CPU interrupt controller.
        NVIC_EnableIRQ(RTC_IRQn);
        NVIC_SetPriority(RTC_IRQn, 0x00);
        
        // Enable interrupt on overflow.
        RTC->MODE1.INTENSET.reg |= RTC_MODE1_INTENSET_OVF;
        RTC_SYNC()
        
        // Enable the RTC module.
        RTC->MODE1.CTRL.reg |= RTC_MODE1_CTRL_ENABLE;
        RTC_SYNC()
        
        mConfigured = true;
    }
}


/**************************************
 * TickZero_::enable
 **************************************/
void TickZero_::enable()
{
    if (mConfigured)
    {
        RTC->MODE1.CTRL.reg |= RTC_MODE1_CTRL_ENABLE; // enable RTC module
        RTC_SYNC()
    }
}


/**************************************
 * TickZero_::disable
 **************************************/
void TickZero_::disable()
{
    RTC->MODE1.CTRL.reg &= ~RTC_MODE1_CTRL_ENABLE;   // disable RTC module
    RTC_SYNC()
}


/**************************************
 * TickZero_::setCount
 **************************************/
void TickZero_::setCount(uint16_t count)
{
    if (mConfigured)
    {
        uint16_t tmpReg = RTC->MODE1.CTRL.reg;
        RTC->MODE1.CTRL.reg &= ~RTC_MODE1_CTRL_ENABLE;   // disable RTC module
        RTC_SYNC()
        RTC->MODE1.COUNT.reg = count;
        RTC_SYNC()
        RTC->MODE1.CTRL.reg = tmpReg;
        RTC_SYNC()
    }
}


/**************************************
 * TickZero_::count
 **************************************/
uint16_t TickZero_::count()
{
    //RTC_SYNC()
    uint16_t myCount = RTC->MODE1.COUNT.reg;
    RTC_SYNC()
    return myCount;
}


/**************************************
 * TickZero_::frac
 **************************************/
float TickZero_::frac()
{
	return (float)count() * FRAC_DENOM;
}


/**************************************
 * TickZero_::millis
 **************************************/
uint16_t TickZero_::millis()
{
    uint32_t temp = (uint32_t)count() * 1000UL;
    return (uint16_t)(temp >> 14); // Assumes max count = 16384
}


/**************************************
 * TickZero_::attachInterrupt
 **************************************/
void TickZero_::attachInterrupt(TickZeroIsrPtr isr)
{
    RTC_callBack = isr;
}


/**************************************
 * TickZero_::detachInterrupt
 **************************************/
void TickZero_::detachInterrupt()
{
    RTC_callBack = 0;
}


/**************************************
 * TickZero_::enableInterrupt
 **************************************/
void TickZero_::enableInterrupt()
{
    NVIC_EnableIRQ(RTC_IRQn);  // Enable overflow interrupt 
}


/**************************************
 * TickZero_::disableInterrupt
 **************************************/
void TickZero_::disableInterrupt()
{
    NVIC_DisableIRQ(RTC_IRQn);  // Disable overflow interrupt (RTC keeps ticking)
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/
 
/**************************************
 * TickZero_::config32kOSC
 **************************************/
void TickZero_::config32kOSC()
{
    SYSCTRL->XOSC32K.reg = SYSCTRL_XOSC32K_ONDEMAND |
                           SYSCTRL_XOSC32K_RUNSTDBY |
                           SYSCTRL_XOSC32K_EN32K |
                           SYSCTRL_XOSC32K_XTALEN |
                           SYSCTRL_XOSC32K_STARTUP(6) |
                           SYSCTRL_XOSC32K_ENABLE;
}


/**************************************
 * TickZero_::configClock
 **************************************/
void TickZero_::configClock()
{
    // Attach peripheral clock 2 to the 32KHz crystal oscillator.
	// Clock division factor = 2^(0+1) = 2, so clock frequency = 32768Hz / 2 = 16384Hz
    GCLK->GENDIV.reg = GCLK_GENDIV_ID(2) | GCLK_GENDIV_DIV(0);
    while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY) {}
    GCLK->GENCTRL.reg = (GCLK_GENCTRL_GENEN | GCLK_GENCTRL_SRC_XOSC32K | GCLK_GENCTRL_ID(2) | GCLK_GENCTRL_DIVSEL );
    while (GCLK->STATUS.reg & GCLK_STATUS_SYNCBUSY) {}
    GCLK->CLKCTRL.reg = (uint32_t)((GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK2 | (RTC_GCLK_ID << GCLK_CLKCTRL_ID_Pos)));
    while (GCLK->STATUS.bit.SYNCBUSY) {}
}


/**************************************
 * TickZero_::RTCreset
 **************************************/
void TickZero_::RTCreset()
{
    RTC->MODE1.CTRL.reg |= RTC_MODE1_CTRL_SWRST; // everything reset to initial state
    RTC_SYNC()
}


/**************************************
 * TickZero_::RTCresetRemove
 **************************************/
void TickZero_::RTCresetRemove()
{
    RTC->MODE1.CTRL.reg &= ~RTC_MODE1_CTRL_SWRST; // clear software reset
    RTC_SYNC()
}


/**************************************
 * RTC_Handler
 * Interrupt handler for the RTC peripheral.
 **************************************/
void RTC_Handler(void)
{
    if (RTC_callBack != 0) 
    {
        RTC_callBack();
    }

    RTC->MODE1.INTFLAG.reg = RTC_MODE1_INTFLAG_OVF; // must clear flag at end
}


// End of file.
