/******************************************************************************
 * TickZero.h
 * Copyright (c) 2017 Thomas Kerr AB3GY
 *
 * Designed for personal use by the author, but available to anyone under
 * the license terms below.
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * One second tick generator using the Arduino Zero ATSAMD21 RTC peripheral.
 */

#ifndef _TICK_ZERO_H
#define _TICK_ZERO_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdint.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
/**
 * @brief Interrupt service routine (ISR) function type definition.
 *
 * Interrupt service routines must be functions that take no arguments
 * (void) and return no value (void).
 *
 * Example: void myIsr(void);
 */
typedef void(*TickZeroIsrPtr)(void);
 
 
/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/
 
/**
 * @class TickZero_
 * @brief One second tick generator using using the Arduino Zero ATSAMD21 
 * RTC peripheral.
 *
 * Provides an accurate 1-second tick generator using a 32.768KHz oscillator
 * and the ATSAMD21 RTC peripheral in count mode.  The user can attach an
 * interrupt service routine (ISR) to execute at 1-second intervals.
 * Methods are provided to provide the current count value as well as a
 * floating-point fractional second value.
 *
 * Only one TickZero_ object should exist in the application since it
 * expects to have exclusive use of the RTC hardware resources.
 */
class TickZero_
{
public:

    /**
     * @brief Default constructor.
     */
    TickZero_();
    
    /**
     * @brief Initialize the RTC hardware and the TickZero_ object.
     *
     * After initialization, the counter is enabled and 1-second interrupts
     * will occur.
     *
     * @return True if initialization successful, false otherwise.
     */
    void begin();
    
    /**
     * @brief Enable the counter.
     *
     * 1-second interrupts will occur unless they have been disabled
     * by the disableInterrupt() method.
     *
     * Note that the counter and interrupts are enabled by the begin() method.
     *
     * See also disable().
     */
    void enable();
    
    /**
     * @brief Disable the counter.
     *
     * See also enable().
     */
    void disable();
    
    /**
     * @brief Set the RTC counter value.
     *
     * The 32.768KHz crystal oscillator is divided by two for counting.
     * The RTC counts from 0 to 16383, and iterrupts occur when the 
     * count value overflows from its maximum value to zero.
     *
     * @param count The count value in the range 0 - 16383.
     */
    void setCount(uint16_t count);
    
    /**
     * @brief Return the counter value.
     *
     * @return The counter value in the range 0 - 16383.
     */
    uint16_t count();
	
    /**
     * @brief Return the counter value in terms of a fraction of a second.
     *
     * @return A floating point value in the range 0.0 <= x < 1.0.
     */
	float frac();
    
    /**
     * @brief Return the counter value in terms of elapsed milliseconds.
     *
     * @return The milliseconds value in the range 0 - 999.
     */
    uint16_t millis();
    
    /**
     * @brief Attach an interrupt service routine (ISR) to the
     * 1-second interrupt.
     *
     * @param isr The ISR function to be called. The function must take no
     * arguments and return no value (i.e. void isr(void)).
     */
    void attachInterrupt(TickZeroIsrPtr isr);
    
    /**
     * @brief Detach the interrupt service routine (ISR) from the 
     * 1-second interrupt.
     *
     * See attachInterrupt() for details.
     */
    void detachInterrupt();
    
    /**
     * @brief Enable interrupts.
     *
     * Enables 1-second interrupts if the counter is enabled.
     *
     * If the counter has been disabled using the disable() method, then
     * no interrupts will occur until the counter has been re-enabled with
     * the enable() method.
     *
     * Note that the counter and interrupts are enabled by the begin() method.
     *
     * See also disableInterrupt().
     */
    void enableInterrupt();
    
    /**
     * @brief Disable 1-second interrupts.  
     *
     * The counter continues to count unless it has been disabled by the
     * disable() method, but no interrupts will occur.
     *
     * See also enableInterrupt().
     */
    void disableInterrupt();
    
private:

    void config32kOSC();    //!< Configure the 32.768KHz crystal oscillator for RTC use
    void configClock();     //!< Configure the peripheral clock system for RTC use
    void RTCreset();        //!< Perform a software reset of the RTC peripheral
    void RTCresetRemove();  //!< Remove the RTC software reset
    
    int mConfigured;  //!< True if RTC hardware has been configured
};

// Define a single TickZero object for use by the application.
extern TickZero_ TickZero;


#endif // _TICK_ZERO_H
