# TickZero #
One second tick generator using using the Arduino Zero ATSAMD21 RTC peripheral.

### Dependencies ###
None.

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License  
https://opensource.org/licenses/MIT
