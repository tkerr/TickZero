/******************************************************************************
 * TickZeroExample.ino
 * Copyright (c) 2019 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
  ******************************************************************************/

/**
 * @file
 * @brief
 * TickZero library example sketch. Prints a message every second.
 */
 
#include <Arduino.h>
#include "TickZero.h"

uint32_t elapsed_seconds = 0;
bool     isr_flag = false;

// TickZero interrupt service routine (ISR).
void tickIsr()
{
    // Don't call Serial functions in an ISR.
    // Set a flag so they can be called in the main loop.
    // Increment the elapsed seconds here.
    elapsed_seconds++;
    isr_flag = true;
}

void setup()
{
    // Turn off the built-in Arduino LED.
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
    
    // Initialize the serial port.
    Serial.begin(9600);
    delay(2000);
    Serial.println("TickZero example sketch.");
    
    // Initialize the TickZero object.
    TickZero.begin();
    TickZero.attachInterrupt(tickIsr);
    TickZero.enable();
}

void loop()
{
    // Wait for TickZero to set a flag.
    if (isr_flag)
    {
        isr_flag = false;
        
        // Delay a random number of milliseconds just to demonstrate
        // the TickZero millis() method.
        delay(random(999));
        
        Serial.print("Elapsed seconds: ");
        Serial.print(elapsed_seconds);
        Serial.print('.');
        Serial.println(TickZero.millis());
    }
}
